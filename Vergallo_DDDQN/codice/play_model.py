import random
from skimage import transform
import gym
import numpy as np
import tensorflow as tf
from collections import deque
import scipy
from gym.spaces import prng
prng.seed(1337)

env = gym.make("MsPacman-v0")

# PARAMS NEURAL NETWKORK
num_episodes = 100
learning_rate = 0.0001
cropped_state_size = [84, 84, 4]
num_input_neurons = cropped_state_size[0]
num_ouptut_neurons = env.action_space.n
common_net_hidden_dimensions = [16, 64]


class DDDQN:
    def __init__(self,
                 session,
                 scope_name,
                 input_size,
                 hidden_layer_sizes,
                 output_size,
                 learning_rate,
                 state_size):

        self.session = session
        self.scope_name = scope_name
        self.input_size = input_size
        self.hidden_layer_sizes = hidden_layer_sizes
        self.output_size = output_size
        self.learning_rate = learning_rate

        with tf.variable_scope(self.scope_name):
            self.inputs_ = tf.placeholder(tf.float32, [None, *state_size], name="inputs")

            # Input is 100x120x1
            self.conv1 = tf.layers.conv2d(inputs=self.inputs_,
                                          filters=32,
                                          kernel_size=[8, 8],
                                          strides=[4, 4],
                                          padding="VALID",
                                          kernel_initializer=tf.contrib.layers.xavier_initializer_conv2d(),
                                          name="conv1",
                                          )

            self.conv1_out = tf.nn.elu(self.conv1, name="conv1_out")

            """
            Second convnet: CNN ELU
            """
            self.conv2 = tf.layers.conv2d(inputs=self.conv1_out,
                                          filters=64,
                                          kernel_size=[4, 4],
                                          strides=[2, 2],
                                          padding="VALID",
                                          kernel_initializer=tf.contrib.layers.xavier_initializer_conv2d(),
                                          name="conv2")

            self.conv2_out = tf.nn.elu(self.conv2, name="conv2_out")

            """
            Third convnet: CNN ELU
            """
            self.conv3 = tf.layers.conv2d(inputs=self.conv2_out,
                                          filters=128,
                                          kernel_size=[4, 4],
                                          strides=[2, 2],
                                          padding="VALID",
                                          kernel_initializer=tf.contrib.layers.xavier_initializer_conv2d(),
                                          name="conv3")

            self.conv3_out = tf.nn.elu(self.conv3, name="conv3_out")

            self.flatten = tf.layers.flatten(self.conv3_out)

            # Separating streams into advantage and value networks
            adv_net = tf.layers.dense(self.flatten, 32, activation=tf.nn.relu)
            adv_net = tf.layers.dense(adv_net, self.output_size)

            val_net = tf.layers.dense(self.flatten, 32, activation=tf.nn.relu)
            val_net = tf.layers.dense(val_net, 1)

            self.output = val_net + (adv_net - tf.reduce_mean(adv_net,
                                                              reduction_indices=1,
                                                              keepdims=True))

            # Placeholder for expected q-values
            self.y = tf.placeholder(shape=[None, self.output_size], dtype=tf.float32)

            # Using the loss method provided by tf directly
            self.loss = tf.losses.mean_squared_error(self.y, self.output)

            self.optimizer = tf.train.RMSPropOptimizer(
                learning_rate=self.learning_rate).minimize(self.loss)

    def predict(self, state):
        return self.session.run(self.output,
                                feed_dict={self.inputs_: state})

    def update(self, state, y):
        return self.session.run([self.loss, self.optimizer],
                                feed_dict={
                                    self.inputs_: state,
                                    self.y: y
                                })

    @staticmethod
    def create_copy_operations(source_scope, dest_scope):
        source_vars = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope=source_scope)
        dest_vars = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope=dest_scope)

        assert len(source_vars) == len(dest_vars)
        result = []

        for source_var, dest_var in zip(source_vars, dest_vars):
            result.append(dest_var.assign(source_var.value()))

        return result


def stack_frames(stacked_frames, state, is_new_episode):
    # Preprocess frame
    frame = preprocess_frame(state)

    if is_new_episode:
        # Clear our stacked_frames
        stacked_frames = deque([np.zeros((cropped_state_size[0], cropped_state_size[1]), dtype=np.int) for i in range(4)], maxlen=4)

        # Because we're in a new episode, copy the same frame 4x
        stacked_frames.append(frame)
        stacked_frames.append(frame)
        stacked_frames.append(frame)
        stacked_frames.append(frame)

        # Stack the frames
        stacked_state = np.stack(stacked_frames, axis=2)

    else:
        # Append frame to deque, automatically removes the oldest frame
        stacked_frames.append(frame)

        # Build the stacked state (first dimension specifies different frames)
        stacked_state = np.stack(stacked_frames, axis=2)

    return stacked_state, stacked_frames


def preprocess_frame(frame):
    # Crop the screen (remove part that contains no information)
    # [Up: Down, Left: right]
    cropped_frame = frame[15:-40, 20:-20]
    rgb = scipy.misc.imresize(cropped_frame, cropped_state_size, interp='bilinear')

    r, g, b = rgb[:, :, 0], rgb[:, :, 1], rgb[:, :, 2]
    gray = 0.2989 * r + 0.5870 * g + 0.1140 * b  # extract luminance

    o = gray.astype('float32') / 128 - 1  # normalize
    o = o.reshape(*o.shape, 1)
    return o


tf.reset_default_graph()

# Initialize deque with zero-images one array for each image
stacked_frames = deque([np.zeros((cropped_state_size[0], cropped_state_size[1]), dtype=np.int) for i in range(4)], maxlen=4)

with tf.Session() as sess:
    main_dqn = DDDQN(session=sess,
                     scope_name="q_main",
                     input_size=num_input_neurons,
                     hidden_layer_sizes=common_net_hidden_dimensions,
                     output_size=num_ouptut_neurons,
                     learning_rate=learning_rate,
                     state_size=cropped_state_size)

    # Saver will help us to load our model
    saver = tf.train.Saver()
    # Load the model
    saver.restore(sess, tf.train.latest_checkpoint('models'))

    for ep_num in range(num_episodes):
        state = env.reset()
        state, stacked_frames = stack_frames(stacked_frames, state, True)
        done = False
        episode_reward, loss, steps = 0, 0, 0

        while not done:
            env.render()
            # select the action
            state_reshaped = np.squeeze(state)
            action = np.argmax(main_dqn.predict(state_reshaped.reshape((1, *state_reshaped.shape))))

            # execute the action
            next_state, reward, done, info = env.step(action)
            next_state, stacked_frames = stack_frames(stacked_frames, next_state, False)

            if done:
                reward = -1

            episode_reward += reward
            steps += 1
            state = next_state
            print("Ep num: ", ep_num, " Reward: ", reward, " Action: ", action)
        print("Episode number: ", ep_num, " with total reward: ", episode_reward)

env.close()