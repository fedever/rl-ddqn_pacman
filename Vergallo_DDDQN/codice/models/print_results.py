import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

csv = pd.read_csv("results2.csv", sep=";")
data = csv[['total_reward', 'episode_number']]
x = data['episode_number']
y = data['total_reward']
plt.plot(x, y)

z = np.polyfit(x, y, 1)
p = np.poly1d(z)
plt.plot(x,p(x),"r--")

plt.show()